require 'spec_helper'
describe "Static Pages" do

  subject { page }

  shared_examples_for "all static pages" do
    it { should have_content(header) }
    it { should have_title(page_title) }
  end
  describe "Home page" do
    before { visit root_path }
    let(:heading) { "Sample App" }
    let(:page_title) { "" }
    it { should have_title("Ruby on Rails Tutorial") }
  end

  describe "Help page" do
    before { visit help_path }
    let(:heading) { "Help" }
    let(:page_title) { "Help" }
  end

  describe "About Page" do
    before { visit about_path }
    let(:heading) { "About" }
    let(:page_title) { "About" }
  end
  describe "Contact Page" do
    before { visit contact_path }
    let(:heading) { "Contact" }
    let(:page_title) { "Contact" }
  end
  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    expect(page).to have_title('About')
    click_link "Help"
    expect(page).to have_title('Help') 
    click_link "Contact"
    expect(page).to have_title('Contact')
    click_link "Home"
    click_link "Sign up now!"
    expect(page).to have_title('Sign Up')
    click_link "sample app"
    expect(page).to have_content("Welcome to") 
  end
end
